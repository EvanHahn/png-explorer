import crel from "./lib/crel.js";
import parsePng from "./parsePng.js";
import formatBytes from "./lib/formatBytes.js";
import pluralize from "./lib/pluralize.js";
import countBytes from "./countBytes.js";
import * as nodePath from "./lib/nodePath.js";
import allChildrenOf from "./lib/allChildrenOf.js";
import NODE_UI_FNS from "./NODE_UI_FNS.js";

const screenEls = document.querySelectorAll(".screen");

const initialState = () => ({
  isLoading: false,
  parsedPng: null,
  hoveringOver: null,
  errorMessage: null,
});

let state = initialState();

uploadScreenEl.addEventListener("submit", (event) => {
  event.preventDefault();
});

uploadInputEl.addEventListener("change", parsePngIfOneExists);

async function parsePngIfOneExists() {
  const file = uploadInputEl.files[0];
  if (!file) return;

  state.isLoading = true;
  render();

  try {
    state.parsedPng = parsePng(await file.arrayBuffer());
    state.errorMessage = null;
  } catch (err) {
    console.error(err);
    state.parsedPng = null;
    state.errorMessage = err.message;
  } finally {
    state.isLoading = false;
    render();
  }
}

explorerBytesEl.addEventListener("click", (event) => {
  const rawPath = event.target.dataset.path;
  if (!rawPath) return;

  for (const explorerEl of allChildrenOf(explorerTreeEl)) {
    if (explorerEl.dataset.path) explorerEl.removeAttribute("open");
  }

  const leafEl = explorerTreeEl.querySelector(
    `details[data-path="${rawPath}"]`,
  );
  let currentTreeEl = leafEl;
  while (currentTreeEl !== explorerTreeEl) {
    currentTreeEl.setAttribute("open", "open");
    currentTreeEl = currentTreeEl.parentNode;
  }
  leafEl.scrollIntoView({ behavior: "smooth" });
});

explorerBytesEl.addEventListener("mousemove", (event) => {
  const path = elPath(event.target);
  if (!path) return;

  state.hoveringOver = { side: "bytes", path };
  render();
});

explorerBytesEl.addEventListener("mouseout", (event) => {
  if (event.target !== explorerBytesEl) return;

  if (state.hoveringOver?.side === "bytes") {
    state.hoveringOver = null;
    render();
  }
});

function onFocusExplorerTree(event) {
  // We don't update the state in these situations to avoid UI jitter.
  if (event.target.classList.contains("children")) {
    return;
  }

  let nodeToCheckForPath = event.target;
  while (nodeToCheckForPath !== explorerTreeEl) {
    if (nodeToCheckForPath.dataset.path) break;
    nodeToCheckForPath = nodeToCheckForPath.parentNode;
  }
  const path = elPath(nodeToCheckForPath);
  if (!path) return;

  state.hoveringOver = { side: "tree", path };
  render();
}

function onBlurExplorerTree(event) {
  if (
    (event.target !== explorerTreeEl) &&
    (event.target !== explorerTreeEl.children[0]) &&
    (state.hoveringOver?.side !== "tree")
  ) return;

  state.hoveringOver = null;
  render();
}

explorerTreeEl.addEventListener("mousemove", onFocusExplorerTree);
explorerTreeEl.addEventListener("focusin", onFocusExplorerTree);
explorerTreeEl.addEventListener("mouseout", onBlurExplorerTree);
explorerTreeEl.addEventListener("focusout", onBlurExplorerTree);

explorerTreeEl.addEventListener("click", (event) => {
  let nodeToCheckForPath = event.target;
  while (nodeToCheckForPath !== explorerTreeEl) {
    if (nodeToCheckForPath.dataset.path) break;
    nodeToCheckForPath = nodeToCheckForPath.parentNode;
  }
  const rawPath = nodeToCheckForPath.dataset.path;
  if (!rawPath) return;

  const bytesEl = explorerBytesEl.querySelector(`[data-path="${rawPath}"]`);
  bytesEl.scrollIntoView({ behavior: "smooth" });
});

function render() {
  let currentScreen;
  if (state.isLoading) {
    currentScreen = loadingScreenEl;
  } else if (!state.parsedPng || state.errorMessage) {
    currentScreen = uploadScreenEl;
    errorMessageEl.innerText = state.errorMessage;
  } else {
    currentScreen = explorerScreenEl;
    renderExplorerScreen(state.parsedPng, state.hoveringOver);
  }
  for (const screenEl of screenEls) {
    if (screenEl === currentScreen) {
      screenEl.removeAttribute("hidden");
    } else {
      screenEl.setAttribute("hidden", "hidden");
    }
  }
}

let previouslyRenderedParsedPng = null;
function renderExplorerScreen(parsedPng, hoveringOver) {
  if (parsedPng !== previouslyRenderedParsedPng) {
    // XXX: Maybe there's a way to combine these two functions into one.
    const bytesEl = (node, path = nodePath.root) => {
      const el = crel("span", { "data-path": path }, []);

      if (node.bytes) {
        el.innerHTML = formatBytes(node.bytes);
      } else {
        node.children.forEach((child, index) => {
          el.appendChild(bytesEl(child, path.adding(index)));
          if (child.bytes) {
            el.appendChild(document.createTextNode(" "));
          }
        });
      }

      return el;
    };

    const treeEl = (node, path = nodePath.root) => {
      const isRoot = path.isEqualTo(nodePath.root);

      const { title, description } = NODE_UI_FNS.get(node.id)(node);
      const byteCount = countBytes(node);

      return crel(
        "details",
        { "data-path": path, open: isRoot },
        [
          crel("summary", {}, [
            crel("span", { "class": "title" }, title),
            crel(
              "span",
              { "class": "bytecount" },
              pluralize(byteCount, "byte"),
            ),
          ]),
          description,
          ...(
            node.children
              ? [crel(
                "div",
                { "class": "children" },
                node.children.map((child, index) => (
                  treeEl(child, path.adding(index))
                )),
              )]
              : []
          ),
        ],
      );
    };

    const backButton = crel("a", { href: "#" }, "Try another?");
    backButton.onclick = (event) => {
      event.preventDefault();
      uploadInputEl.value = null;
      state = initialState();
      render();
    };

    try {
      explorerBytesEl.innerHTML = "";
      explorerBytesEl.appendChild(bytesEl(parsedPng));

      explorerTreeEl.innerHTML = "";
      explorerTreeEl.appendChild(treeEl(parsedPng));
      explorerTreeEl.appendChild(backButton);

      previouslyRenderedParsedPng = parsedPng;
    } catch (err) {
      console.error(err);
      state.parsedPng = null;
      state.errorMessage = typeof err === "string" ? err : err.message;
      render();
      return;
    }
  }

  let isByteElActivated;
  let isExplorerElActivated;
  switch (hoveringOver?.side) {
    case undefined:
      isByteElActivated = isExplorerElActivated = () => false;
      break;
    case "bytes":
      isByteElActivated = (path) => path.isEqualTo(hoveringOver.path);
      isExplorerElActivated = (path) => path.isSupersetOf(hoveringOver.path);
      break;
    case "tree":
      isByteElActivated = (path) => hoveringOver.path.isSupersetOf(path);
      isExplorerElActivated = (path) => path.isEqualTo(hoveringOver.path);
      break;
    default:
      throw new Error("Unexpected hovering over");
  }

  for (const bytesEl of allChildrenOf(explorerBytesEl.children[0])) {
    const path = elPath(bytesEl);
    bytesEl.classList.toggle("activated", isByteElActivated(path));
  }
  for (const explorerEl of allChildrenOf(explorerTreeEl)) {
    const path = elPath(explorerEl);
    if (!path) continue;
    explorerEl.classList.toggle("activated", isExplorerElActivated(path));
  }
}

function elPath(el) {
  return nodePath.parse(el.dataset.path);
}

render();
parsePngIfOneExists();
