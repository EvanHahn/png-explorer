import { Id } from "./constants.js";

const PNG_SIGNATURE = new Uint8Array([137, 80, 78, 71, 13, 10, 26, 10]);

/**
 * @typedef {Object} Node
 * @property {Id} id
 * @property {Uint8Array=} bytes
 * @property {Array<Node>=} children
 */

/**
 * @param {ArrayBuffer} buf The bytes of an image.
 * @returns {Node}
 */
export default function parsePng(buf) {
  return {
    id: Id.root,
    children: [
      { id: Id.signature, bytes: PNG_SIGNATURE },
      ...getChunks(buf),
    ],
  };
}

function* getChunks(buf) {
  const bytes = new Uint8Array(buf);
  const view = new DataView(buf);

  checkSignature(bytes.subarray(0, 8));

  for (let i = PNG_SIGNATURE.length; i < bytes.length;) {
    const length = view.getUint32(i);
    i += 4;

    const typeBytes = bytes.subarray(i, i + 4);
    const type = parseType(typeBytes);
    i += 4;

    const data = bytes.subarray(i, i + length);
    assert(data.length === length, "Invalid data. Did the file end too early?");
    i += length;

    const crc = bytes.subarray(i, i + 4);
    checkCrc(crc, typeBytes, data);
    i += 4;

    yield formatChunk(type, data, crc);
  }
}

function assert(condition, message) {
  if (!condition) {
    throw new Error(message);
  }
}

function checkSignature(bytes) {
  assert(
    areBytesEqual(PNG_SIGNATURE, bytes),
    "Invalid PNG signature. Is this a PNG file?",
  );
}

function parseType(bytes) {
  assert(bytes.length === 4, "Invalid chunk type (invalid length)");
  const result = new TextDecoder().decode(bytes);
  assert(
    /^[A-Za-z]{4}$/.test(result),
    "Invalid chunk type (contains invalid characters)",
  );
  return result;
}

const crcTable = [];
for (let i = 0; i < 256; i++) {
  let b = i;
  for (let j = 0; j < 8; j++) {
    b = b & 1 ? 0xedb88320 ^ (b >>> 1) : b >>> 1;
  }
  crcTable[i] = b >>> 0;
}

function checkCrc(sourceCrc, typeBytes, data) {
  const allData = new Uint8Array([...typeBytes, ...data]);

  let crc = -1;
  for (let i = 0; i < allData.length; i++) {
    crc = crcTable[(crc ^ allData[i]) & 0xff] ^ (crc >>> 8);
  }
  crc ^= -1;

  const expectedBuffer = new ArrayBuffer(4);
  new DataView(expectedBuffer).setUint32(0, crc);
  const expectedCrc = new Uint8Array(expectedBuffer);

  assert(
    areBytesEqual(sourceCrc, expectedCrc),
    "Invalid chunk data (checksum failed)",
  );
}

function areBytesEqual(a, b) {
  if (a.length !== b.length) {
    return false;
  }

  for (let i = 0; i < a.length; i++) {
    if (a[i] !== b[i]) {
      return false;
    }
  }

  return true;
}

function formatChunk(type, data, crc) {
  let id = Id.unknownChunk;
  let chunkLength = { id: Id.chunkLength, bytes: pngUint(data.byteLength) };
  const chunkType = { id: Id.chunkType, bytes: pngType(type) };
  let chunkData = { id: Id.chunkData, bytes: data };
  const chunkCrc = { id: Id.chunkCrc, bytes: crc };

  switch (type) {
    case "IHDR":
      id = Id.ihdr;
      chunkData = {
        id: Id.ihdrChunkData,
        children: [
          { id: Id.ihdrWidth, bytes: data.subarray(0, 4) },
          { id: Id.ihdrHeight, bytes: data.subarray(4, 8) },
          { id: Id.ihdrBitDepth, bytes: data.subarray(8, 9) },
          { id: Id.ihdrColourType, bytes: data.subarray(9, 10) },
          { id: Id.ihdrCompressionMethod, bytes: data.subarray(10, 11) },
          { id: Id.ihdrFilterMethod, bytes: data.subarray(11, 12) },
          { id: Id.ihdrInterlaceMethod, bytes: data.subarray(12, 13) },
        ],
      };
      break;
    case "PLTE":
      id = Id.plte;
      chunkData = {
        id: Id.plteChunkData,
        children: chunkBytes(data, 3).map((bytes) => ({
          id: Id.plteColor,
          bytes,
        })),
      };
      break;
    case "IDAT":
      id = Id.idat;
      chunkData = { ...chunkData, id: Id.idatChunkData };
      break;
    case "IEND":
      id = Id.iend;
      chunkLength = { ...chunkLength, id: Id.iendChunkLength };
      break;
    case "tRNS":
      id = Id.trns;
      break;
    case "cHRM":
      id = Id.chrm;
      break;
    case "gAMA":
      id = Id.gama;
      break;
    case "iCCP":
      id = Id.iccp;
      break;
    case "sBIT":
      id = Id.sbit;
      break;
    case "sRGB":
      id = Id.srgb;
      break;
    case "cICP":
      id = Id.cicp;
      break;
    case "tEXt": {
      id = Id.text;
      const nullSeparatorIndex = data.indexOf(0);
      if (nullSeparatorIndex === -1) break;
      chunkData = {
        id: Id.textData,
        children: [
          { id: Id.textKeyword, bytes: data.subarray(0, nullSeparatorIndex) },
          {
            id: Id.textNullSeparator,
            bytes: data.subarray(nullSeparatorIndex, nullSeparatorIndex + 1),
          },
          { id: Id.textString, bytes: data.subarray(nullSeparatorIndex + 1) },
        ],
      };
      break;
    }
    case "zTXt": {
      id = Id.ztxt;
      const nullSeparatorIndex = data.indexOf(0);
      if (nullSeparatorIndex === -1) break;

      chunkData = {
        id: Id.ztxtData,
        children: [
          { id: Id.textKeyword, bytes: data.subarray(0, nullSeparatorIndex) },
          {
            id: Id.textNullSeparator,
            bytes: data.subarray(nullSeparatorIndex, nullSeparatorIndex + 1),
          },
          {
            id: Id.ztxtCompressionMethod,
            bytes: data.subarray(
              nullSeparatorIndex + 1,
              nullSeparatorIndex + 2,
            ),
          },
          { id: Id.ztxtString, bytes: data.subarray(nullSeparatorIndex + 2) },
        ],
      };
      break;
    }
    case "iTXt":
      id = Id.itxt;
      break;
    case "bKGD":
      id = Id.bkgd;
      break;
    case "hIST":
      id = Id.hist;
      break;
    case "pHYs":
      id = Id.phys;
      break;
    case "sPLT":
      id = Id.splt;
      break;
    case "eXIf":
      id = Id.exif;
      break;
    case "tIME":
      id = Id.time;
      break;
    case "acTL":
      id = Id.actl;
      break;
    case "fcTL":
      id = Id.fctl;
      break;
    case "fdAT":
      id = Id.fdat;
      break;
    case "oFFs":
      id = Id.offs;
      break;
    case "pCAL":
      id = Id.pcal;
      break;
    case "sCAL":
      id = Id.scal;
      break;
    case "gIFg":
      id = Id.gifg;
      break;
    case "gIFx":
      id = Id.gifx;
      break;
    case "gIFt":
      id = Id.gift;
      break;
    case "sTER":
      id = Id.ster;
      break;
    case "dSIG":
      id = Id.dsig;
      break;
    case "iDOT":
      id = Id.idot;
      break;
    default:
      console.warn(`Unknown chunk type ${type}`);
      break;
  }

  const children = [];

  children.push(chunkLength);
  children.push(chunkType);
  if (data.byteLength) children.push(chunkData);
  children.push(chunkCrc);

  return { id, children };
}

function pngUint(n) {
  const resultBuffer = new ArrayBuffer(4);
  new DataView(resultBuffer).setUint32(0, n);
  return new Uint8Array(resultBuffer);
}

function pngType(type) {
  return new TextEncoder().encode(type);
}

function chunkBytes(bytes, size) {
  const result = [];
  for (let i = 0; i < bytes.byteLength; i += size) {
    result.push(bytes.subarray(i, i + size));
  }
  return result;
}
