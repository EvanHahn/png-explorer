export default function parsePngUint(bytes) {
  const dataView = new DataView(
    bytes.buffer,
    bytes.byteOffset,
    bytes.byteLength,
  );
  return dataView.getUint32(0);
}
