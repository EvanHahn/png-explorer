/**
 * @param {number} count
 * @param {string} noun
 * @returns {string}
 */
export default function pluralize(count, noun) {
  let result = count + " " + noun;
  if (count !== 1) result += "s";
  return result;
}
