export default function* allChildrenOf(node) {
  yield node;
  for (const child of node.children || []) yield* allChildrenOf(child);
}
