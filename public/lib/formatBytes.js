const MAX_BYTES_PER_SECTION = 256;

/**
 * @param {Uint8Array} bytes
 * @returns {string}
 */
export default function formatBytes(bytes) {
  let result = "";
  let hasWrittenFirstByte = false;

  // We truncate for performance.
  //
  // There are other ways to achieve this but this is the simplest.
  const shouldTruncate = bytes.byteLength > MAX_BYTES_PER_SECTION;
  if (shouldTruncate) {
    bytes = bytes.subarray(0, MAX_BYTES_PER_SECTION);
  }

  for (const byte of bytes) {
    if (hasWrittenFirstByte) {
      result += " ";
    } else {
      hasWrittenFirstByte = true;
    }
    result += byte.toString(16).padStart(2, "0");
  }

  if (shouldTruncate) {
    result += " … ";
  }

  return result;
}
