export default function crel(tagName, attributes, children) {
  const result = document.createElement(tagName);

  for (const [key, value] of Object.entries(attributes)) {
    if (value !== false) result.setAttribute(key, value);
  }

  if (typeof children === "string") {
    result.innerText = children;
  } else {
    for (const child of children) {
      result.append(child);
    }
  }

  return result;
}
