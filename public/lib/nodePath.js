class NodePath {
  #path;

  constructor(path) {
    this.#path = path;
  }

  adding(index) {
    return new NodePath([...this.#path, index]);
  }

  isEqualTo(other) {
    return this.#path.length === other.#path.length && this.isSupersetOf(other);
  }

  isSupersetOf(other) {
    for (let i = 0; i < this.#path.length; i++) {
      if (this.#path[i] !== other.#path[i]) return false;
    }
    return true;
  }

  toString() {
    return this.#path.join(".");
  }
}

export const root = new NodePath(["$"]);

export function parse(value) {
  if (typeof value !== "string") return;
  return new NodePath(
    value.split(".").map((rawIndex) =>
      rawIndex === "$" ? rawIndex : Number(rawIndex)
    ),
  );
}
