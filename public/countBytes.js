export default function countBytes(node) {
  if (node.bytes) return node.bytes.byteLength;
  else {
    return node.children.reduce(
      (result, child) => result + countBytes(child),
      0,
    );
  }
}
