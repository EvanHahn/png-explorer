/** @enum {string} */
export const Id = Object.create(null);
[
  "root",
  "signature",

  "unknownChunk",
  "chunkLength",
  "chunkType",
  "chunkData",
  "chunkCrc",

  "ihdr",
  "ihdrChunkData",
  "ihdrWidth",
  "ihdrHeight",
  "ihdrBitDepth",
  "ihdrColourType",
  "ihdrCompressionMethod",
  "ihdrFilterMethod",
  "ihdrInterlaceMethod",

  "plte",
  "plteChunkData",
  "plteColor",

  "idat",
  "idatChunkData",

  "iend",
  "iendChunkLength",

  "trns",

  "chrm",

  "gama",

  "iccp",

  "sbit",

  "srgb",

  "cicp",

  "text",
  "textData",
  "textKeyword",
  "textNullSeparator",
  "textString",

  "ztxt",
  "ztxtData",
  "ztxtCompressionMethod",
  "ztxtString",

  "itxt",

  "bkgd",

  "hist",

  "phys",

  "splt",

  "exif",

  "time",

  "actl",

  "fctl",

  "fdat",

  "offs",

  "pcal",

  "scal",

  "gifg",

  "gifx",

  "gift",

  "ster",

  "dsig",

  "idot",
].forEach((id) => {
  Id[id] = id;
});
