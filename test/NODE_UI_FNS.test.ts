import { assert } from "https://deno.land/std@0.183.0/testing/asserts.ts";
import { Id } from "../public/constants.js";
import NODE_UI_FNS from "../public/NODE_UI_FNS.js";

Deno.test("node UI functions are defined for all IDs", () => {
  const ids = new Set(Object.values(Id));

  for (const id of ids) {
    assert(NODE_UI_FNS.has(id), `NODE_UI_FNS should have ${id}`);
  }

  for (const key of NODE_UI_FNS.keys()) {
    assert(
      ids.has(key),
      `${key}, in NODE_UI_FNS, should also be an ID. Value: ${
        NODE_UI_FNS.get(key)?.toString()
      }`,
    );
  }
});
