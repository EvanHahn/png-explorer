import {
  assert,
  assertEquals,
  assertThrows,
} from "https://deno.land/std@0.183.0/testing/asserts.ts";
import { Id } from "../public/constants.js";
import parsePng from "../public/parsePng.js";

const FIXTURES_URL = new URL("./fixtures/", import.meta.url);

async function getFixture(name: string): Promise<ArrayBuffer> {
  const fixtureUrl = new URL(name, FIXTURES_URL);
  return (await Deno.readFile(fixtureUrl)).buffer;
}

async function* getFixtureNames(): AsyncIterable<string> {
  for await (const dirEntry of Deno.readDir(FIXTURES_URL)) {
    if (!dirEntry.isFile) continue;
    const { name } = dirEntry;
    if (name.endsWith(".png")) yield name;
  }
}

const b = (bytes: Iterable<number>) => new Uint8Array(bytes);
const t = (type: string) => new TextEncoder().encode(type);

Deno.test("parses PNG test suite reasonably", async () => {
  const fixturesToSkip = new Set([
    // Incorrect bit depths, which we don't detect.
    "xd0n2c08.png",
    "xd3n2c08.png",
    "xd9n2c08.png",
    // Incorrect color types, which we don't detect.
    "xc1n0g08.png",
    "xc9n2c08.png",
    // Missing an IDAT chunk, which we don't detect.
    "xdtn0g01.png",
  ]);

  for await (const fixtureName of getFixtureNames()) {
    const fixture = await getFixture(fixtureName);

    if (fixturesToSkip.has(fixtureName)) continue;

    if (fixtureName.startsWith("x")) {
      assertThrows(
        () => parsePng(fixture),
        `${fixtureName} should fail to parse`,
      );
    } else {
      try {
        parsePng(fixture);
      } catch {
        assert(false, `${fixtureName} should parse`);
      }
    }
  }
});

Deno.test("parses a basic PNG", async () => {
  const fixture = await getFixture("basn2c16.png");
  const parsed = parsePng(fixture);

  assertEquals(parsed, {
    id: Id.root,
    children: [
      { id: Id.signature, bytes: b([137, 80, 78, 71, 13, 10, 26, 10]) },
      {
        id: Id.ihdr,
        children: [
          { id: Id.chunkLength, bytes: b([0, 0, 0, 13]) },
          { id: Id.chunkType, bytes: t("IHDR") },
          {
            id: Id.ihdrChunkData,
            children: [
              { id: Id.ihdrWidth, bytes: b([0, 0, 0, 32]) },
              { id: Id.ihdrHeight, bytes: b([0, 0, 0, 32]) },
              { id: Id.ihdrBitDepth, bytes: b([16]) },
              { id: Id.ihdrColourType, bytes: b([2]) },
              { id: Id.ihdrCompressionMethod, bytes: b([0]) },
              { id: Id.ihdrFilterMethod, bytes: b([0]) },
              { id: Id.ihdrInterlaceMethod, bytes: b([0]) },
            ],
          },
          { id: Id.chunkCrc, bytes: b([172, 136, 49, 224]) },
        ],
      },
      {
        id: "gama",
        children: [
          { id: Id.chunkLength, bytes: b([0, 0, 0, 4]) },
          { id: Id.chunkType, bytes: t("gAMA") },
          { id: Id.chunkData, bytes: b([0, 1, 134, 160]) },
          { id: Id.chunkCrc, bytes: b([49, 232, 150, 95]) },
        ],
      },
      {
        id: "idat",
        children: [
          { id: Id.chunkLength, bytes: b([0, 0, 0, 229]) },
          { id: Id.chunkType, bytes: t("IDAT") },
          {
            id: Id.idatChunkData,
            bytes: new Uint8Array(fixture, 8 + 25 + 16 + 8, 229),
          },
          { id: Id.chunkCrc, bytes: b([7, 187, 196, 236]) },
        ],
      },
      {
        id: Id.iend,
        children: [
          { id: Id.iendChunkLength, bytes: b([0, 0, 0, 0]) },
          { id: Id.chunkType, bytes: t("IEND") },
          { id: Id.chunkCrc, bytes: b([0xae, 0x42, 0x60, 0x82]) },
        ],
      },
    ],
  });
});
