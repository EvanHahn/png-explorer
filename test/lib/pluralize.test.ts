import { assertEquals } from "https://deno.land/std@0.183.0/testing/asserts.ts";
import pluralize from "../../public/lib/pluralize.js";

Deno.test("singular", () => {
  assertEquals(pluralize(1, "cake"), "1 cake");
});

Deno.test("not 1", () => {
  assertEquals(pluralize(-1, "cake"), "-1 cakes");
  assertEquals(pluralize(0, "cake"), "0 cakes");
  assertEquals(pluralize(2, "cake"), "2 cakes");
});
