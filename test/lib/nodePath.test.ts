import {
  assert,
  assertEquals,
} from "https://deno.land/std@0.183.0/testing/asserts.ts";
import { parse, root } from "../../public/lib/nodePath.js";

Deno.test("round trips", () => {
  ["$", "$.1", "$.0.1.2.3"].forEach((str) => {
    assertEquals(parse(str)?.toString(), str);
  });
});

Deno.test("adding + equality checking", () => {
  const viaParse = parse("$.1.2");
  const viaAdding = root.adding(1).adding(2);
  assert(viaParse?.isEqualTo(viaAdding));
  assert(viaAdding.isEqualTo(viaParse));
});

Deno.test("supersets", () => {
  assert(parse("$")?.isSupersetOf(parse("$")));
  assert(parse("$")?.isSupersetOf(parse("$.2")));
  assert(parse("$")?.isSupersetOf(parse("$.2.3")));

  assert(parse("$.1.2")?.isSupersetOf(parse("$.1.2")));
  assert(parse("$.1.2")?.isSupersetOf(parse("$.1.2.3")));
  assert(!parse("$.1.2")?.isSupersetOf(parse("$.1")));
  assert(!parse("$.1.2")?.isSupersetOf(parse("$.1.22")));
});
