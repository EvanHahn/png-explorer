import { assertEquals } from "https://deno.land/std@0.183.0/testing/asserts.ts";
import { Id } from "../public/constants.js";

Deno.test("IDs don't have duplicates", () => {
  const ids = Object.values(Id);
  const uniqueIds = new Set(ids);
  assertEquals(ids.length, uniqueIds.size);
});
